import { useEffect, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';




function ShoeDetail() {
    const [shoe, setShoe] = useState([])
    //pass shoe id from APP.js
    const {id} = useParams()
    //redirect
    const nav = useNavigate()

    const getData = async () => {
      const response = await fetch(`http://localhost:8080/api/shoes/${id}/`);


      if (response.ok) {
        const data = await response.json();
        setShoe(data)
      }
    }

    useEffect(()=>{
      getData()
    }, [])


    //delete function
    async function deleteShoe() {
        await fetch(`http://localhost:8080/api/shoes/${id}/`, { method: 'DELETE' });
        alert('Delete successful');
        nav("/shoes/List")
    }

    if(shoe.bin === undefined) {
        return(
            <p>Loading</p>
        )
    }

    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model Name</th>
            <th>Color</th>
            <th>Closet Name</th>
            <th>Bin Number</th>
            <th>Bin Size</th>
            <th>Picture</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
              <tr key={shoe.id}>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.model_name }</td>
                <td>{ shoe.color }</td>
                <td>{ shoe.bin.closet_name }</td>
                <td>{ shoe.bin.bin_size }</td>
                <td>{ shoe.bin.bin_size }</td>
                <td>
                    <img
                        src={shoe.picture_url}
                        width={100} height={100}
                        alt='shoe picture'
                    />
                </td>
                <td>
                    <button onClick={() => deleteShoe()}>Delete</button>
                </td>
              </tr>

        </tbody>
      </table>
    );
}

export default ShoeDetail
