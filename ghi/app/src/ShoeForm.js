import React, {useState, useEffect} from 'react';

function ShoeForm() {
    //get bin inforamtion
    const [bins, setBins] = useState([])

    const getData = async () => {
        const binurl = 'http://localhost:8080/api/bins/';
        const response = await fetch(binurl);

    if (response.ok) {
        const data = await response.json();
        setBins(data.bins);
        }
    }

    useEffect(() => {
        getData();
    }, []);

    //create form & handle form change
    const [formData, setFormData] = useState({
        manufacturer: '',
        model_name: '',
        color: '',
        picture_url: '',
        bin: '',
    })

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
        //Previous form data is spread (i.e. copied) into our new state object
            ...formData,

        //On top of the that data, we add the currently engaged input key and value
            [inputName]: value
        });
    }

    //handle submit
    const handleSubmit = async (event) => {
        event.preventDefault();

        const shoeUrl = 'http://localhost:8080/api/shoes/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(shoeUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                manufacturer: '',
                model_name: '',
                color: '',
                picture_url: '',
                bin: '',
            });
        }
    }

    async function Message() {
        alert('Create successful');
    }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new shoe</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
              <label htmlFor="manufacturer">manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.model_name} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control" />
              <label htmlFor="model_name">model_name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">color</label>
            </div>
            <div className="mb-3">
              <label htmlFor="Picture url">picture_url</label>
              <textarea onChange={handleFormChange} value={formData.picture_url} className="form-control" id="picture_url" rows="3" name="picture_url" className="form-control"></textarea>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.bin} required name="bin" id="bin" className="form-select">
                <option value="">Choose a bin</option>
                {bins.map(bin => {
                  return (
                    <option key={bin.import_href} value={bin.import_href}>{bin.closet_name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary" onClick={() => Message()}> Create
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ShoeForm;
