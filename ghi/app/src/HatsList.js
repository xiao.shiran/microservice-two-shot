
import { useState, useEffect} from 'react';

function HatsList() {
    const [hat, setHats] = useState([])

    const getData = async ()=> {
        const response = await fetch ('http://localhost:8090/api/hats');
        if (response.ok) {
            const {hat} = await response.json();
            setHats(hat);

        } else{
            console.error('An error occured fetching the data')
        }
    }
    console.log(hat.ID)
    const handleDelete = (hatId) => {

        fetch(`http://localhost:8090/api/hats/${hatId}/`, { method: 'DELETE' })
            .then(() => {
                setHats(hat.filter(hat => hat.id !== hatId));
            })
            .catch(error => console.error('Error deleting hat:', error));
        };

    useEffect(()=> {
        getData()
    }, []);

    return (
        <div className="column">
        <div className="row">
            <h1>Current Hats</h1>

            <table className="table">
            <thead>
                <tr>
                <th>Style Name</th>
                <th>Fabric</th>
                <th>Color</th>
                <th>Location</th>
                <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {hat.map(hat => {
                return (
                    <tr key={hat.id}>
                        <td>{ hat.style_name }</td>
                        <td>{ hat.fabric }</td>
                        <td>{ hat.color }</td>
                        <td>{ hat.location.closet_name }</td>
                        <td>
                            <button onClick={() => handleDelete(hat.id)}>Delete</button>
                        </td>
                    </tr>
                );
                })}
            </tbody>
            </table>
        </div>
        </div>
    );
}

export default HatsList;
